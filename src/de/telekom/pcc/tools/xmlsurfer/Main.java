package de.telekom.pcc.tools.xmlsurfer;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final String LS = "ls";
    private static final String UP = "up";
    private static final String GO = "go";
    private static final String SHOW = "show";
    private static final String GREP = "grep";
    private static final String EXIT = "exit";
    private static final int LS_END_DEFAULT = 20;

    private static Node currentNode;
    private static List<Node> childNodes;

    public static void main(String[] args) throws ParserConfigurationException {
        if (args.length < 1) {
            System.out.println("Format: xmlsurfer FILE");
            return;
        }
        var file = new File(args[0]);
        if (!file.exists()) {
            System.out.println("File not found");
            return;
        }

        var factory = DocumentBuilderFactory.newInstance();
        var builder = factory.newDocumentBuilder();
        System.out.println("Parsing XML...");
        Document document;
        try {
            document = builder.parse(file);
        } catch (SAXException | IOException e) {
            System.out.println("Error occurred during XML parsing:");
            e.printStackTrace();
            return;
        }
        System.out.println("Done.");

        currentNode = document.getChildNodes().item(0);
        childNodes = getChildNodes(currentNode);
        show();
        ls();
        var scanner = new Scanner(System.in);
        do {
            System.out.print("XML-Surfer> ");
        } while (read(scanner.nextLine()));
    }

    private static void ls() {
        ls(0, LS_END_DEFAULT);
    }

    private static void ls(int begin, int end) {
        int from = Math.max(0, begin);
        int to = Math.min(childNodes.size(), end);
        if (from > 0) {
            System.out.println("...");
        }
        for (int i = from; i < to; i++) {
            Node item = childNodes.get(i);
            System.out.println(i + ". " + renderNode(item));
        }
        if (to < childNodes.size()) {
            System.out.println("...");
            System.out.println("Size: " + childNodes.size());
        }
    }

    private static void up() {
        if (currentNode.getParentNode() == null) {
            System.out.println("Current node is root");
            return;
        }
        currentNode = currentNode.getParentNode();
        childNodes = getChildNodes(currentNode);
        show();
    }

    private static void go(int i) {
        if (i >= childNodes.size()) {
            System.out.println("Node size: " + childNodes.size());
            return;
        }
        currentNode = childNodes.get(i);
        childNodes = getChildNodes(currentNode);
        show();
        ls();
    }

    private static void show() {
        System.out.println(renderNode(currentNode));
    }

    private static void grep(String pattern, List<Node> nodes, String prefix) {
        for (int i = 0; i < nodes.size(); i++) {
            var node = nodes.get(i);
            String renderedNode = renderNode(node);
            if (renderedNode.matches(".*" + pattern + ".*")) {
                System.out.println(prefix + i + ". " + renderedNode);
            }
            if (node.hasChildNodes()) {
                grep(pattern, getChildNodes(node), prefix + i + ".");
            }
        }
    }

    private static String renderNode(Node node) {
        var result = new StringBuilder("<");
        result.append(node.getNodeName());
        var attributes = node.getAttributes();
        if (attributes != null) {
            for (int i = 0; i < attributes.getLength(); i++) {
                result.append(" ");
                result.append(attributes.item(i));
            }
        }
        result.append(node.hasChildNodes() ? ">" : "/>");
        if (node.hasChildNodes()) {
            result.append(renderNodeContent(node));
        }
        return result.toString();
    }

    private static String renderNodeContent(Node node) {
        String textContent = null;
        if (node.getChildNodes().getLength() == 1) {
            var item = node.getChildNodes().item(0);
            textContent = item.getTextContent();
        }
        return textContent == null ? "" : textContent;
    }

    private static boolean read(String line) {
        String[] cmd = line.strip().split("\\s+");
        switch (cmd[0].toLowerCase()) {
            case LS:
                try {
                    int begin = 0;
                    if (cmd.length > 1) {
                        begin = Integer.parseInt(cmd[1]);
                    }
                    int end = begin + LS_END_DEFAULT;
                    if (cmd.length > 2) {
                        end = Integer.parseInt(cmd[2]);
                    }
                    ls(begin, end);
                } catch (NumberFormatException e) {
                    System.out.println("Format: " + LS + " [BEGIN] [END]");
                }
                System.out.println();
                break;

            case UP:
                up();
                System.out.println();
                break;

            case GO:
                try {
                    int target = Integer.parseInt(cmd[1]);
                    go(target);
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                    System.out.println("Format: " + GO + " CHILD_INDEX");
                }
                System.out.println();
                break;

            case SHOW:
                show();
                System.out.println();
                break;

            case GREP:
                if (cmd.length != 2) {
                    System.out.println("Format: " + GREP + " PATTERN");
                    break;
                }
                grep(cmd[1], childNodes, "");
                System.out.println();
                break;

            case "":
                break;

            case EXIT:
                System.out.println("Bye!");
                System.out.println();
                return false;

            default:
                System.out.println("Available commands:");
                System.out.println(LS + " [BEGIN] [END]");
                System.out.println(UP);
                System.out.println(GO + " CHILD_INDEX");
                System.out.println(SHOW);
                System.out.println(GREP + " PATTERN");
                System.out.println(EXIT);
                System.out.println();
        }

        return true;
    }

    private static List<Node> getChildNodes(Node node) {
        var childNodes = node.getChildNodes();
        var result = new ArrayList<Node>();
        for (int i = 0; i < childNodes.getLength(); i++) {
            var item = childNodes.item(i);
            if (item.getNodeType() != Node.TEXT_NODE) {
                result.add(item);
            }
        }
        return result;
    }

}
